const mongoose = require('mongoose');
// const mongooseSerial = require("mongoose-serial");


const dataSchema = new mongoose.Schema({
    email_id: {
        type: String,
        unique: true,
        required: true,
        sparse:true
    },
    password: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    address: {
        type: String,
        required: true
    },
    age: {
        type: Number,
        required: true
    },
    birthday: {
        type: String,
        required: true
    },
    profession: {
        type: String,
        required: true
    },
    achievement: {
        type: String,
        required: true
    }
})

module.exports = mongoose.model('Data', dataSchema);