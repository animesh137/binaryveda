const express = require('express');
const bodyParser = require("body-parser")
const app = express();
const signup = require('../src/controllers/signup');
const login = require('../src/controllers/login');
const mongoose = require('mongoose');
const dotenv = require('dotenv');
dotenv.config();


// app.use(express.json())
app.engine('html', require('ejs').renderFile);
app.use(express.static('./views'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
    extended: true
}))


//connection to mongodb
const connection = mongoose.connect(process.env.DB_URL, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }).then(r => console.log("Connected to MongoDB!"))
    .catch(e => console.log("Error Connecting: ", e))


//POST
//signup
app.post('/signup', async (req, res) => {
    signup(req, res);
});

//login
app.post('/login', async (req, res) => {
    login(req, res);
});

//GET
//running the server
app.get("/", (req, res) => {
    res.set({
        "Allow-access-Allow-Origin": '*'
    })
    return res.render('../src/views/signup.html');
}).listen(3000, () => {
    console.log('Server Running on 3000!');
});

//to get the login page
app.get("/login_page", (req, res) => {
    // return res.redirect('/signup');
    return res.render('../src/views/login.html');
});