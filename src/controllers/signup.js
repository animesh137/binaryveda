const bcrypt = require("bcrypt")
const Data = require("../models/user_data");
const Joi = require("joi");
const dotenv = require("dotenv");
dotenv.config();
const alert = require("alert");

async function signup(req, res) {
    const {
        email_id,
        password,
        name,
        address,
        age,
        birthday,
        profession,
        achievement
    } = req.body;

    const error = validateData(req.body);
    if (error.error) {
        //can be further improved with exact err message
        return res.status(400).send("Invalid Data! Please crosscheck!")
    }

    if (!(name && address && age && birthday && email_id && password && profession && achievement)) {
        return res.send("All the details are necessary!");
    }

    const alreadyExists = await Data.findOne({
        email_id
    })

    //Check if user already exists
    if (alreadyExists) {
        alert("User Already Exist. Please try Logging in.")
        return res.redirect('/login_page');
    }

    //Encrypt Password
    const salt = await bcrypt.genSalt(10);
    const encryptedPassword = await bcrypt.hash(password, salt);

    const userAdd = await Data.create({
        email_id: email_id.toLowerCase(),
        password: encryptedPassword,
        name: name.toLowerCase(),
        address: address.toLowerCase(),
        age: age,
        birthday: birthday,
        profession: profession,
        achievement: achievement
    })
    return res.send(userAdd.name + " added to the DB!");
}

const validateData = (body) => {
    const schema = Joi.object({
        email_id: Joi.string().required().email(),
        password: Joi.string().required(),
        name: Joi.string().required(),
        address: Joi.string().required(),
        age: Joi.number().required(),
        birthday: Joi.date().required(),
        profession: Joi.string().required(),
        achievement: Joi.string().required()
    })
    return schema.validate(body);
}

module.exports = signup;