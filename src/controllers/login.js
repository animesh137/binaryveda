const bcrypt = require("bcrypt")
const Data = require('../models/user_data');
const dotenv = require('dotenv');
dotenv.config();
const jwt = require("jsonwebtoken");

async function login(req, res) {
    const {
        email_id,
        password,
    } = req.body;

    if (!email_id) {
        return res.send("Email-ID Missing!");
    }
    if (!password) {
        return res.send("Password Missing!");
    }

    // Validate if user exist in our database
    const user = await Data.findOne({
        email_id
    });

    if (user && (await bcrypt.compare(password, user.password))) {
        const token = jwt.sign({
                email_id,
                password
            },
            process.env.KEY
        );

        //send jwt in header
        let payload = `<!DOCTYPE html>
        <html lang="en">
        <head>
            User Data
        </head>
        <body>
            <h4>Name: ${user.name}</h4>
            <h4>Address: ${user.address}</h4>
            <h4>Age: ${user.age}</h4>
            <h4>Birthday: ${user.birthday}</h4>
            <h4>Profession: ${user.profession}</h4>
            <h4>Achievement: ${user.achievement}</h4>
        </body>
        </html>`;

        return res.setHeader('jwt-token', token).send(payload);
    }
    return res.send("Invalid Credentials").status(401);
}


module.exports = login;